import { Injectable } from '@nestjs/common';
let Client = require('ssh2-sftp-client');
@Injectable()
export class AppService {

  getHello(): string {
    return 'Hello World! everywhere';
  }


  getSSH(){
    const sftp = new Client('test-ovh-connect');
     sftp.connect({
       host:`${process.env.URL_SRV}`,
       port:22,
       username: `${process.env.USER_OVH}`,
       password: `${process.env.PASSWOR_USER_OVH}`
    })
    .then(()=> 
    {console.info('connected from OVH CLOUD');
    sftp.list('/')
    })
    .catch((err)=>{ console.info(err.message, 'catch error')});
}

async onModuleInit():Promise<void>{
  await this.getSSH();
}


}
